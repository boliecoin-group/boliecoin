
Debian
====================
This directory contains files used to package bolied/bolie-qt
for Debian-based Linux systems. If you compile bolied/bolie-qt yourself, there are some useful files here.

## bolie: URI support ##


bolie-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install bolie-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your bolie-qt binary to `/usr/bin`
and the `../../share/pixmaps/bolie128.png` to `/usr/share/pixmaps`

bolie-qt.protocol (KDE)

