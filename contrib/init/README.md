Sample configuration files for:

SystemD: bolied.service
Upstart: bolied.conf
OpenRC:  bolied.openrc
         bolied.openrcconf

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
